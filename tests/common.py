def make_url_parts(url):
    return {
        name: getattr(url, name)
        for name in ('scheme', 'host', 'port', 'path', 'username', 'password')
    }
